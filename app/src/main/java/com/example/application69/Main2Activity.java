package com.example.application69;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void onButtonClick(View view){
        Log.i("Hello","Hello there");
        Button p1_button = (Button)findViewById(R.id.prevButton);
        p1_button.setText("Hello there");

        switch(view.getId()) {
            case R.id.prevButton:
                finish();
                Log.i("Activity Closing","Closing Main2Activity");
                break;
            case R.id.nextButton:
                startActivity(new Intent(Main2Activity.this, Main4Activity.class));
                Log.i("Activity Opening","Opening Main4Activity");
                break;
            default:
                Log.e("Button ID","Unknow button ID");
        }
    }

}
